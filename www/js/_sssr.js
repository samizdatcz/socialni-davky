
//vstupy
var	obec = 0,
	byt = false,
	ubytovna = false,
	trvalyPobytJinde = 0,
	duchody = 0,
	pocetKojencu = 0,
	pocetBatolat = 0,
	pocetPredskolaku = 0,
	pocetSkolaku = 0,
	pocetStudentu = 0,
	pocetDospelych = 0,
	pocetRodicu = 0,
	pocetDeti = 0,
	pocetDuchodcu = 0,
	najem = 0,
	energie = 0,
	hrubyPrijemRodicu = [],
	hrubyPrijemDospelych = [],
	prijemDuchodcu = [];

//výstupy
var cistyPrijemDomacnostiZeZamestnani = 0,
	rodicovska = 0,
	pridavkyNaDeti = 0,
	prispevekNaBydleni = 0,
	prispevekNaZivobyti = 0,
	doplatekNaBydleni = 0,
	nakladyNaBydleni = 0,
	disponibilniPrijemPoOdecteniNakladuNaBydleni = 0,
	disponibilniPrijemNaCloveka = 0,
	lineData = [];

var	cistyPrijemDomacnostiZeZamestnaniGraf = 0,
	disponibilniPrijemPoOdecteniNakladuNaBydleniGraf = 0,
	disponibilniPrijemNaClovekaGraf = 0,
		duchodyGraf = 0,
		rodicovskaGraf = 0,
		pridavkyNaDetiGraf = 0,
		prispevekNaBydleniGraf = 0,
		prispevekNaZivobytiGraf = 0,
		doplatekNaBydleniGraf = 0,
		nakladyNaBydleniGraf = 0;

function addItem($block, config) {

	var config = $.extend({
		name: $block.data("name"),
		step: $block.data("step"),
		value: $block.data("default-value"),
		max: $block.data("max"),
		min: $block.data("min"),
		label: $block.data("label")
	}, config);

	var step = config.step || 1;

	$block.find(".items").append('<li>' + config.label +': <div class="input-group">'
		+ '<input type="number" class="form-control" step="' + step + '"' +
		' min="' + config.min + '" max="' + config.max + '" name="'
		+ config.name + '[]" value="' + config.value + '">'
		+ '<span class="input-group-addon" style="padding: 0;">'
		+'<a href="" class="jsRemoveItem btn btn-default">×</a></span></div></li>');

	checkForMax($block);
}

function valuesChanged() {

	var isOk = true;

	$rodiceForm = $("#rodiceForm .items li");
	pocetRodicu = $rodiceForm.length;
	hrubyPrijemRodicu = $.map($rodiceForm, function(el) {
		var platRodicu = $(el).find("input").val();
		if(platRodicu != 0 && platRodicu < 11000) {
			alert("Minimální hrubý příjem je 11 000 Kč, u nezaměstnaných bez nároku na podporu zadejte 0");
			isOk = false;
		}
		if(platRodicu > 30000) {
			alert("Kalkulačka je určena pro nízkopříjmové kategorie, zadejte nižší plat než 30 tisíc korun");
			isOk = false;
		}
		return platRodicu;
	});

	$dospeliForm = $("#dospeliForm .items li");
	pocetDospelych = $dospeliForm.length;
	hrubyPrijemDospelych = $.map($dospeliForm, function(el) {
		var platDospelych = $(el).find("input").val();
		if(platDospelych != 0 && platDospelych < 11000) {
			alert("Minimální hrubý příjem je 11 000 Kč, u nezaměstnaných bez nároku na podporu zadejte 0");
			isOk = false;
		}
		if(platDospelych > 112928) {
			alert("Kalkulačka je určena pro nízkopříjmové kategorie, zadejte nižší plat než 30 tisíc korun");
			isOk = false;
		}
		return platDospelych;
	});

	$detiForm = $("#detiForm .items li");
	pocetDeti = $detiForm.length;

	pocetKojencu = 0; pocetBatolat = 0; pocetPredskolaku = 0; pocetSkolaku = 0;

	for (i = 0; i < pocetDeti; i++) {
		var vek = Number($detiForm.eq(i).find("input").val());
		if (vek <= 0.75) {
			pocetKojencu++;
		} else if (vek <= 4) {
			pocetBatolat++;
		} else if (vek <= 6) {
			pocetPredskolaku++;
		} else {
			pocetSkolaku++;
		}
	}

	pocetStudentu = Number(document.getElementsByName("studentiInput")[0].value)

	$duchodciForm = $("#duchodciForm .items li");
	pocetDuchodcu = $duchodciForm.length;

	prijemDuchodcu = $.map($duchodciForm, function(el) {
		var platDuchodcu = $(el).find("input").val();
		if(platDuchodcu > 28575) {
			alert("Maximální možná výše důchodu v roce 2017 je 28 575 Kč");
			isOk = false;
		}
		return platDuchodcu;
	});

	obec = document.getElementsByName("obecInput")[0].value;

	typBydleni = $("#typBydleni").val();

	byt = (typBydleni == "bytVlastni")

	ubytovna = (typBydleni == "ubytovna")

	najem = Number(document.getElementsByName("najemInput")[0].value);

	energie = Number(document.getElementsByName("energieInput")[0].value)

	trvalyPobytJinde = Number(document.getElementsByName("trvalyInput")[0].value)

	return isOk;

}

function finalCountdown(modelovyPrijemDomacnosti, bezDavek) {

	// výpočet příjmu domácnosti pro graf: skutečný hrubý příjem je nahrazen hypotetickým hrubým příjmem
	if (modelovyPrijemDomacnosti != null) {
		hrubyPrijemRodicu[0] = modelovyPrijemDomacnosti;
	}

	// výpočet čistých příjmů domácnosti ze zaměstnání
	var	socialniRodicu = [],
		zdravotniRodicu = [],
		zalohaNaDanRodicu = [],
		socialniDospelych = [],
		zdravotniDospelych = [],
		zalohaNaDanDospelych = [],
		cistyPrijemRodicu = [],
		cistyPrijemDospelych = [],
		pocetNezaopatrenych = 0,
		slevaNaPoplatnika = 2070,
		danoveZvyhodneniNaDeti = 0,
		danovyBonusNaDeti = 0,
		cistyPrijemDomacnostiBezBonusuNaDeti = 0;

	cistyPrijemDomacnostiZeZamestnani = 0;

	pocetNezaopatrenych = pocetKojencu + pocetBatolat + pocetPredskolaku + pocetSkolaku + pocetStudentu;

	if ((hrubyPrijemRodicu[0] > 0) || (hrubyPrijemRodicu[1] > 0)) {
		if (pocetNezaopatrenych == 1) {
			danoveZvyhodneniNaDeti = 1117;
		} else if (pocetNezaopatrenych == 2) {
			danoveZvyhodneniNaDeti = 1117 + 1417;
		} else if (pocetNezaopatrenych > 2) {
			danoveZvyhodneniNaDeti = Math.min(1117 + 1417 + (pocetNezaopatrenych - 2) * 1717, 5025);
		}
	}

	for (i = 0; i < hrubyPrijemRodicu.length; i++) {
		socialniRodicu[i] = 0.065 * hrubyPrijemRodicu[i];
		zdravotniRodicu[i] = 0.045 * hrubyPrijemRodicu[i];
		zalohaNaDanRodicu[i] = Math.ceil(0.15 * 1.34 * hrubyPrijemRodicu[i] / 10) * 10;
		cistyPrijemRodicu[i] = hrubyPrijemRodicu[i] - socialniRodicu[i] - zdravotniRodicu[i] - Math.max(zalohaNaDanRodicu[i] - slevaNaPoplatnika, 0);
	}

	if ((hrubyPrijemRodicu[0] >=hrubyPrijemRodicu[1]) || (hrubyPrijemRodicu[1] == undefined)) {		//u lépe placeného rodiče výpočet daňového bonusu/slevy na dítě
		cistyPrijemRodicu[0] = cistyPrijemRodicu[0] + danoveZvyhodneniNaDeti;
		danovyBonusNaDeti = Math.max(danoveZvyhodneniNaDeti - Math.max(zalohaNaDanRodicu[0] - slevaNaPoplatnika, 0), 0)
	} else {
		cistyPrijemRodicu[1] = cistyPrijemRodicu[1] + danoveZvyhodneniNaDeti;
		danovyBonusNaDeti = Math.max(danoveZvyhodneniNaDeti - Math.max(zalohaNaDanRodicu[1] - slevaNaPoplatnika, 0), 0)
	}

	for (i = 0; i < hrubyPrijemDospelych.length; i++) {
		socialniDospelych[i] = 0.065 * hrubyPrijemDospelych[i];
		zdravotniDospelych[i] = 0.045 * hrubyPrijemDospelych[i];
		zalohaNaDanDospelych[i] = Math.ceil(0.15 * 1.34 * hrubyPrijemDospelych[i] / 10) * 10;
		cistyPrijemDospelych[i] = hrubyPrijemDospelych[i] - socialniDospelych[i] - zdravotniDospelych[i] - Math.max(zalohaNaDanDospelych[i] - slevaNaPoplatnika, 0);
	}

	for(i = 0; i < hrubyPrijemRodicu.length; i++) {
		cistyPrijemDomacnostiZeZamestnani = cistyPrijemDomacnostiZeZamestnani + cistyPrijemRodicu[i];
	}
	for(i = 0; i < hrubyPrijemDospelych.length; i++) {
		cistyPrijemDomacnostiZeZamestnani = cistyPrijemDomacnostiZeZamestnani + cistyPrijemDospelych[i];
	}

	cistyPrijemDomacnostiBezBonusuNaDeti = cistyPrijemDomacnostiZeZamestnani - danovyBonusNaDeti;


	// výpočet důchodů (součet vstupů)
	duchody = 0;

	for (i = 0; i < prijemDuchodcu.length; i++) {
		duchody = duchody + Number(prijemDuchodcu[i]);
	}

	// výpočet rodičovské
	rodicovska = 0;

	rodicovska = Math.min(7600, 7600 * pocetKojencu + 3800 * pocetBatolat);

	// výpočet přídavků na děti
	var zivotniMinimumOsamely = 0,
		zivotniMinimumPrvni = 0,
		zivotniMinimumDalsiDospeli = 0,
		zivotniMinimum15az26 = 0,
		zivotniMinimum6az15 = 0,
		zivotniMinimumPod6 = 0,
		pridavkyNaDetiPod6 = 0,
		pridavkyNaDeti6az15 = 0,
		pridavkyNaDeti15az26 = 0,
		zivotniMinimum = 0,

	pridavkyNaDeti = 0;

	if (((pocetRodicu + pocetDospelych) == 1) && ((pocetDeti + pocetStudentu + pocetDuchodcu) == 0)) {
		zivotniMinimumOsamely = 3410;
	}

	if (((pocetRodicu + pocetDospelych) > 1) || (((pocetRodicu + pocetDospelych) == 1)  && ((pocetDeti + pocetStudentu + pocetDuchodcu) > 0))) {
		zivotniMinimumPrvni = 3140;
	}

	zivotniMinimumDalsiDospeli = 2830 * (pocetRodicu + pocetDospelych - 1);

	zivotniMinimum15az26 = 2450 * pocetStudentu;

	zivotniMinimum6az15 = 2140 * pocetSkolaku;

	zivotniMinimumPod6 = 1740 * (pocetKojencu + pocetBatolat + pocetPredskolaku);

	zivotniMinimum = zivotniMinimumOsamely + zivotniMinimumPrvni + zivotniMinimumDalsiDospeli + zivotniMinimum15az26 + zivotniMinimum6az15 + zivotniMinimumPod6;

	if (cistyPrijemDomacnostiZeZamestnani < 2.4 * zivotniMinimum) {
		pridavkyNaDetiPod6 = 500 * (pocetKojencu + pocetBatolat + pocetPredskolaku);
	}

	if (cistyPrijemDomacnostiZeZamestnani < 2.4 * zivotniMinimum) {
		pridavkyNaDeti6az15 = 610 * pocetSkolaku;
	}

	if (cistyPrijemDomacnostiZeZamestnani < 2.4 * zivotniMinimum) {
		pridavkyNaDeti15az26 = 700 * pocetStudentu;
	}

	pridavkyNaDeti = pridavkyNaDetiPod6 + pridavkyNaDeti6az15 + pridavkyNaDeti15az26;

	// příspěvek na bydlení
	var clenuSpolecneDomacnosti = 0,
		normativniNakladyNaBydleni = 0,
		koeficientNakladuNaBydleni = 0,
		primereneNakladyNaBydleniProPrispevekNaBydleni = 0,
		skutecneNakladyNaBydleni = 0,
		normativniNajem = 0,
		narokNaPrispevekNaBydleni = false,
		rozhodnyPrijemProPrispevekNaBydleni = 0;

	prispevekNaBydleni = 0;

	clenuSpolecneDomacnosti = pocetRodicu + pocetDospelych + pocetDuchodcu + pocetNezaopatrenych - trvalyPobytJinde;

	rozhodnyPrijemProPrispevekNaBydleni = Math.max(cistyPrijemDomacnostiBezBonusuNaDeti + duchody + rodicovska + pridavkyNaDeti, zivotniMinimum);

	if (obec == 4) {
		koeficientNakladuNaBydleni = 0.35;
	} else {
		koeficientNakladuNaBydleni = 0.3;
	}

	if (byt) {
		switch(clenuSpolecneDomacnosti) {
			case 1: normativniNajem = 1944; break;
			case 2: normativniNajem = 2660; break;
			case 3: normativniNajem = 3478; break;
			default: normativniNajem = 4194;
		}
	}

	if (byt) {
		nakladyNaBydleniProPrispevekNaBydleni = Math.min(najem, normativniNajem) + energie;
	} else {
		nakladyNaBydleniProPrispevekNaBydleni = najem + energie;
	}

	if (byt) {
		switch(clenuSpolecneDomacnosti) {
			case 1: normativniNakladyNaBydleni = 4357; break;
			case 2: normativniNakladyNaBydleni = 6429; break;
			case 3: normativniNakladyNaBydleni = 8880; break;
			default: normativniNakladyNaBydleni = 11244;
		}
	} else {
		if (obec == 4) {
				switch (clenuSpolecneDomacnosti) {
					case 1: normativniNakladyNaBydleni = 7720; break;
					case 2: normativniNakladyNaBydleni = 11004; break;
					case 3: normativniNakladyNaBydleni = 14896; break;
					default: normativniNakladyNaBydleni = 18577;
				}
			} else if (obec == 3) {
				switch (clenuSpolecneDomacnosti) {
					case 1: normativniNakladyNaBydleni = 6114; break;
					case 2: normativniNakladyNaBydleni = 8806; break;
					case 3: normativniNakladyNaBydleni = 12022; break;
					default: normativniNakladyNaBydleni = 15112;
				}
			} else if (obec == 2) {
				switch (clenuSpolecneDomacnosti) {
					case 1: normativniNakladyNaBydleni = 5822; break;
					case 2: normativniNakladyNaBydleni = 8407; break;
					case 3: normativniNakladyNaBydleni = 11500; break;
					default: normativniNakladyNaBydleni = 14482;
				}
			} else if (obec == 1) {
				switch (clenuSpolecneDomacnosti) {
					case 1: normativniNakladyNaBydleni = 4950; break;
					case 2: normativniNakladyNaBydleni = 7213; break;
					case 3: normativniNakladyNaBydleni = 9939; break;
					default: normativniNakladyNaBydleni = 12599;
				}
			} else { // obec == 0
				switch (clenuSpolecneDomacnosti) {
					case 1: normativniNakladyNaBydleni = 4763; break;
					case 2: normativniNakladyNaBydleni = 6957; break;
					case 3: normativniNakladyNaBydleni = 9604; break;
					default: normativniNakladyNaBydleni = 12195;
				}
			}
		}

	primereneNakladyNaBydleniProPrispevekNaBydleni = koeficientNakladuNaBydleni * rozhodnyPrijemProPrispevekNaBydleni;

	narokNaPrispevekNaBydleni = (!ubytovna && (nakladyNaBydleniProPrispevekNaBydleni > primereneNakladyNaBydleniProPrispevekNaBydleni) && (normativniNakladyNaBydleni > primereneNakladyNaBydleniProPrispevekNaBydleni));

	skutecneNakladyNaBydleni = Math.min(nakladyNaBydleniProPrispevekNaBydleni, normativniNakladyNaBydleni);

	if (narokNaPrispevekNaBydleni) {
		prispevekNaBydleni = skutecneNakladyNaBydleni - primereneNakladyNaBydleniProPrispevekNaBydleni;
	}

	// příspěvek na živobytí
	var maximalniPrijemProPrispevekNaZivobyti = 0,
		primereneNakladyNaBydleniProPrispevekNaZivobyti = 0,
		oduvodneneNakladyNaBydleniProPrispevekNaZivobyti = 0,
		existencniMinimum = 2200,
		rozhodnyPrijemProPrispevekNaZivobyti = 0;

	prispevekNaZivobyti = 0;

	nakladyNaBydleni = najem + energie;

	castkaZivobyti = zivotniMinimum; // v některých případech může být na existenčním minimu

	rozhodnyPrijemProPrispevekNaZivobyti = 0.7 * cistyPrijemDomacnostiBezBonusuNaDeti + 0.8 * duchody + rodicovska + pridavkyNaDeti;

	oduvodneneNakladyNaBydleniProPrispevekNaZivobyti = Math.min(nakladyNaBydleni, 0.75 * normativniNakladyNaBydleni);

	primereneNakladyNaBydleniProPrispevekNaZivobyti = Math.min(koeficientNakladuNaBydleni * rozhodnyPrijemProPrispevekNaZivobyti, oduvodneneNakladyNaBydleniProPrispevekNaZivobyti);

	prispevekNaZivobyti = Math.max(castkaZivobyti - (cistyPrijemDomacnostiBezBonusuNaDeti - primereneNakladyNaBydleniProPrispevekNaZivobyti), 0);

	// výpočet doplatku na bydlení
	var oduvodneneNakladyNaBydleni = 0,
		najemProDoplatekNaBydleni = 0,
		rozhodnyPrijemProDoplatekNaBydleni = 0;

	doplatekNaBydleni = 0;

	rozhodnyPrijemProDoplatekNaBydleni = 0.7 * cistyPrijemDomacnostiBezBonusuNaDeti + 0.8 * duchody + rodicovska + pridavkyNaDeti + prispevekNaBydleni + prispevekNaZivobyti;

	najemProDoplatekNaBydleni = najem; // ve skutečnosti je nájem zastropován obvyklým nájmem pro každý kraj, který určuje ÚP

	if (ubytovna) {
		oduvodneneNakladyNaBydleni = Math.min(najemProDoplatekNaBydleni + energie, 0.9 * normativniNakladyNaBydleni); // na ubytovně nebo rekreačním zařízení je strop 90 % normativu na nájemní bydlení
	} else {
		oduvodneneNakladyNaBydleni = Math.min(najemProDoplatekNaBydleni + energie, normativniNakladyNaBydleni); // ve vlastním bytě nebo sociálním zařízení je strop normativ na vlastní bydlení
	}

	if (prispevekNaZivobyti > 0) { // pouze při nároku na základní dávku v hmotné nouzi
		doplatekNaBydleni = Math.max((oduvodneneNakladyNaBydleni - prispevekNaBydleni) - Math.max(cistyPrijemDomacnostiBezBonusuNaDeti - castkaZivobyti, 0), 0)
	}

	cistyPrijemDomacnostiZeZamestnani = Math.round(cistyPrijemDomacnostiZeZamestnani, 0)
	duchody = Math.round(duchody, 0)
	rodicovska = Math.round(rodicovska, 0)
	pridavkyNaDeti = Math.round(pridavkyNaDeti, 0)
	prispevekNaBydleni = Math.round(prispevekNaBydleni, 0)
	prispevekNaZivobyti = Math.round(prispevekNaZivobyti, 0)
	doplatekNaBydleni = Math.round(doplatekNaBydleni, 0)
	nakladyNaBydleni = Math.round(nakladyNaBydleni, 0)

	disponibilniPrijemPoOdecteniNakladuNaBydleni = 0;

	disponibilniPrijemPoOdecteniNakladuNaBydleni = cistyPrijemDomacnostiZeZamestnani + duchody + rodicovska + pridavkyNaDeti + prispevekNaBydleni + prispevekNaZivobyti + doplatekNaBydleni - nakladyNaBydleni;

	disponibilniPrijemNaCloveka = Math.round(disponibilniPrijemPoOdecteniNakladuNaBydleni/(clenuSpolecneDomacnosti + trvalyPobytJinde), 0);

	if (bezDavek) {
		disponibilniPrijemPoOdecteniNakladuNaBydleni = cistyPrijemDomacnostiZeZamestnani + duchody - nakladyNaBydleni;
	}

	if (modelovyPrijemDomacnosti == null) {
		cistyPrijemDomacnostiZeZamestnaniGraf = cistyPrijemDomacnostiZeZamestnani
		duchodyGraf = duchody
		rodicovskaGraf = rodicovska
		pridavkyNaDetiGraf = pridavkyNaDeti
		prispevekNaBydleniGraf = prispevekNaBydleni
		prispevekNaZivobytiGraf = prispevekNaZivobyti
		doplatekNaBydleniGraf = doplatekNaBydleni
		nakladyNaBydleniGraf = nakladyNaBydleni
		disponibilniPrijemNaClovekaGraf = disponibilniPrijemNaCloveka
		disponibilniPrijemPoOdecteniNakladuNaBydleniGraf = disponibilniPrijemPoOdecteniNakladuNaBydleni
	}

	if(modelovyPrijemDomacnosti == 0) {
		/**console.log(castkaZivobyti)
		console.log(rozhodnyPrijemProDoplatekNaBydleni)
		console.log(cistyPrijemDomacnostiZeZamestnani)
		console.log(duchody)
		console.log(rodicovska)
		console.log(pridavkyNaDeti)
		console.log(prispevekNaBydleni)
		console.log(prispevekNaZivobyti)
		console.log(doplatekNaBydleni)
		console.log(nakladyNaBydleni)
		console.log(disponibilniPrijemPoOdecteniNakladuNaBydleni)
		console.log(disponibilniPrijemNaCloveka)**/
	}

	return disponibilniPrijemPoOdecteniNakladuNaBydleni;

}

var panelCounter = 0;

function addPanel(text, description) {

	description = description || '';

	var panel = "";
	panel = '<div class="panel panel-default"> \
    <div class="panel-heading" role="tab" id="heading'+panelCounter+'"> \
      <h4 class="panel-title">';

    panel += text;

    if(description) {
    	panel += '<a class="collapsed simulator-help" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'+panelCounter+'" aria-expanded="true" aria-controls="collapse'+panelCounter+'">?</a>';
    }
    panel += '</h4></div>';

    if(description) {
    	panel += ' <div id="collapse'+panelCounter+'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading'+panelCounter+'"> \
	      <div class="panel-body"> \
	        ' + description + ' \
	      </div> \
	    </div>'
    }


  panel += '</div>';

  panelCounter++;

  return panel;
}

function uka() {

	// vynulovat všechy globální proměnné
	cistyPrijemDomacnostiZeZamestnani = 0,
	rodicovska = 0,
	pridavkyNaDeti = 0,
	prispevekNaBydleni = 0,
	prispevekNaZivobyti = 0,
	doplatekNaBydleni = 0,
	nakladyNaBydleni = 0,
	disponibilniPrijemPoOdecteniNakladuNaBydleni = 0,
	duchody = 0,
	lineData = [],
	lineDataBezPrace = [],
	lineDataBezDavek = [];

	finalCountdown();

	var hrubyPrijemProGraf = hrubyPrijemRodicu[0];

	function numberWithCommas(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
	}

	var text = '<h3>Příjmy domácnosti</h3>';
	text += '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';

	var desc = 'Čistý příjem rodičů může být díky daňovému bonusu na dítě vyšší než hrubá mzda.'

	text += addPanel("Čistý příjem domácnosti: " + numberWithCommas(cistyPrijemDomacnostiZeZamestnaniGraf) +" Kč", desc)

/*	text += '<div>';
	text += "<h4 data-toggle='tooltip' title='Tady bude tooltip'>Čistý příjem domácnosti bez bonusu na děti: " + numberWithCommas(cistyPrijemDomacnostiBezBonusuNaDeti) +" Kč <span class='label label-info'>?</span></h4>"
	text += '</div>';

	text += '<div>';
	text += "<h4 data-toggle='tooltip' title='Tady bude tooltip'>Daňové zvýhodnění na děti: " + numberWithCommas(danoveZvyhodneniNaDeti) +" Kč <span class='label label-info'>?</span></h4>"
	text += '</div>';

	text += '<div>';
	text += "<h4 data-toggle='tooltip' title='Tady bude tooltip'>Daňový bonus na děti: " + numberWithCommas(danovyBonusNaDeti) +" Kč <span class='label label-info'>?</span></h4>"
	text += '</div>'; */

	text += addPanel("Důchody: " + numberWithCommas(duchodyGraf) +" Kč");

	desc = 'U rodin, které si platí nemocenské pojištění a mají pravidelné příjmy, navazuje rodičovská na mateřskou a rodiče si můžou nastavit\
		 její měsíční výši. U domácností na hranici životního minima je rodičovská téměř vždy stejná: v prvních devíti měsících pobírají 7 600 korun,\
		 do čtyř let dítěte 3 800 korun. Celkem – stejně jako bohatší rodiny – mohou vybrat 220 tisíc korun. Pokud se před vybráním dávky narodí\
		 další dítě, nárok na dávku za to starší zaniká.<br>\
		 <a href="https://portal.mpsv.cz/soc/ssp/obcane/rodicovsky_prisp">Rodičovský příspěvek na webu MPSV</a>'

	text += addPanel("Rodičovský příspěvek: " + numberWithCommas(rodicovskaGraf) +" Kč", desc);

	desc = '<a href="https://portal.mpsv.cz/soc/ssp/obcane/prid_na_dite">Přídavek na dítě na webu MPSV</a>'

	text += addPanel("Přídavky na děti: " + numberWithCommas(pridavkyNaDetiGraf) +" Kč", desc);

	desc = 'Na příspěvek na bydlení má nárok domácnost, u které náklady spojené s bydlením spolknou víc než 30 procent (v Praze 35 procent)\
		 příjmů. Nájem, energie a podobné výdaje zároveň nesmí překračovat strop, určený počtem lidí v domácnosti a velikostí obce.\
		 Členové rodiny s trvalým pobytem zapsaným jinde se do domácnosti nepočítají. Na dávku má nárok nájemce nebo vlastník bytu,\
		 u majitelů bytu je ovšem strop nákladů velmi nízký. Pokud žije rodina v podnájmu, dávka jde majiteli bytu. Rodiny na ubytovně\
		 na příspěvek na bydlení nemají nárok vůbec.<br>\
		 <a href="https://portal.mpsv.cz/soc/ssp/obcane/prisp_na_bydleni">Příspěvek na bydlení na webu MPSV</a>'

	text += addPanel("Příspěvek na bydlení: " + numberWithCommas(prispevekNaBydleniGraf) +" Kč", desc);

/*	text += '<div>';
	text += "<h4 data-toggle='tooltip' title='Tady bude tooltip'>Rozhodný příjem pro příspěvek na bydlení: " + numberWithCommas(rozhodnyPrijemProPrispevekNaBydleni) +" Kč <span class='label label-info'>?</span></h4>"
	text += '</div>';

	text += '<div>';
	text += "<h4 data-toggle='tooltip' title='Tady bude tooltip'>Životní minimum: " + numberWithCommas(zivotniMinimum) +" Kč <span class='label label-info'>?</span></h4>"
	text += '</div>'; */

	desc = 'Na příspěvek na živobytí mají nárok pouze ty domácnosti, kterým po zaplacení nájmu zbude méně než životní minimum,\
		 mezi 1 740 korunami u malých dětí a 3 410 korunami u samostatně žijících dospělých. Nárok na dávku nemá rodina s úsporami nebo majetkem,\
		 který by mohla prodat nebo pronajmout. Od letošního února musejí žadatelé o dávku vykonávat veřejně prospěšné práce v rozsahu \
		 alespoň dvaceti hodin měsíčně. Pokud odmítnou, úředník může dávku snížit na takzvané existenční minimum, které je u 2 020 korun.<br>\
		 <a href="https://portal.mpsv.cz/soc/hn/obcane/zivobyti">Příspěvek na živobytí na webu MPSV</a>'

	text += addPanel("Příspěvek na živobytí: " + numberWithCommas(prispevekNaZivobytiGraf) +" Kč", desc);

/*	text += '<div>';
	text += "<h4 data-toggle='tooltip' title='Tady bude tooltip'>Rozhodný příjem pro příspěvek na živobytí: " + numberWithCommas(rozhodnyPrijemProPrispevekNaZivobyti) +" Kč <span class='label label-info'>?</span></h4>"
	text += '</div>'; */

	desc = 'Doplatek na bydlení má zaručit, že po zaplacení nákladů na bydlení rodině zůstane existenční minimum. Jde o jednu z nejčastěji\
	 kritizovaných dávek a často se také mění pravidla pro její přidělování. Doplatek na bydlení – na rozdíl od příspěvku na bydlení – není vázán\
	 na trvalé bydliště, proto může sloužit například k zaplacení ubytoven. V různých regionech má navíc různý strop a mírně odlišná pravidla\
	 pro získání, některé úřady mu v minulosti dávaly přednost před nájemním bydlením.<br>\
	 <a href="https://portal.mpsv.cz/soc/hn/obcane/bydleni">Doplatek na bydlení na webu MPSV</a>'

	text += addPanel("Doplatek na bydlení: " + numberWithCommas(doplatekNaBydleniGraf) +" Kč", desc);

/*	text += '<div>';
	text += "<h4 data-toggle='tooltip' title='Tady bude tooltip'>Rozhodný příjem pro doplatek na bydlení: " + numberWithCommas(rozhodnyPrijemProDoplatekNaBydleni) +" Kč <span class='label label-info'>?</span></h4>"
	text += '</div>'; */

	text += addPanel("Náklady na bydlení: " + numberWithCommas(nakladyNaBydleniGraf) +" Kč");

	text += addPanel("= Disponibilní příjem po odečtení nákladů na bydlení: " + numberWithCommas(disponibilniPrijemPoOdecteniNakladuNaBydleniGraf) + " Kč (" + numberWithCommas(disponibilniPrijemNaClovekaGraf) + " Kč na hlavu)");

	text += '</div>'; // closes <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

	$("#results").html(text);
	$("#chart").html("");

	//d3.select("svg").remove()

	var w = 600,
		h = 400,
		padding = 100,
		toppadding = 20;

	var svg = d3.select("#chart")
		.append("svg")
		.attr("width", w)
		.attr("height", h)
		.attr("id","vypocet")

	svg.append("rect")
		.attr("width", "95%")
		.attr("height", "100%")
		.attr("fill", "white")

	var dolniMez = 11000,
		horniMez = Math.max(2 * hrubyPrijemProGraf, 22000);

	for (j = dolniMez; j <= horniMez; j = j + 1000) {
		lineData.push({x: j, y: finalCountdown(j)});
		lineDataBezDavek.push({x: j, y: finalCountdown(j, bezDavek = true)});
	}

	lineDataBezPrace = {x: 0, y: finalCountdown(0)};

	var xMax = 0,
		yMax = 0;
	for (i = 0; i < lineData.length; i++) {
		if(lineData[i]['x'] > xMax) {xMax = lineData[i]['x']}
		if(lineData[i]['y'] > yMax) {yMax = lineData[i]['y']}
	}

	var xScale = d3.scale.linear()
		.domain([0, xMax])
		.range([padding, w - padding]);

	var yScale = d3.scale.linear()
		.domain([0, 1.2 * yMax])
		.range([h - padding, padding]);

	var negativeXScale = d3.scale.linear()
		.domain([padding, w - padding])
		.range([0, xMax]);

	var negativeYScale = d3.scale.linear()
		.domain([h - padding, padding])
		.range([0, 1.2 * yMax]);

	var xAxis = d3.svg.axis()
		.orient("bottom")
		.scale(xScale)
		.ticks(4)
		.tickValues([Math.round(xMax/5/1000)*1000, Math.round(2*xMax/5/1000)*1000, Math.round(3*xMax/5/1000)*1000, Math.round(4*xMax/5/1000)*1000])

	var yAxis = d3.svg.axis()
		.scale(yScale)
		.orient("left")
		.ticks(4)

	var chartline = d3.svg.line()
		.x(function(d) {
			return xScale(d.x);
		})
		.y(function(d) {
			return yScale(d.y);
		})
		.interpolate("linear");

	svg.append("g")
			.attr("class", "x axis")
			.attr("transform", "translate(0," + (h - padding) + ")")
			.call(xAxis)
		.selectAll("text")
			.attr("x", -30)
			.attr("y", 0)
			.attr("transform", "rotate(-45)")

	svg.append("text")
			.attr("x", w/2)
			.attr("y", yScale(0)+80)
			.style("text-anchor", "middle")
			.text("Hrubý příjem prvního rodiče (Kč)")

	svg.append("g")
			.attr("class", "y axis")
			.attr("transform", "translate(" + padding + ",0)")
			.call(yAxis)
		.append("text")
			.attr("x", 15)
			.attr("y", 80)
			.attr("transform", "translate(-150,230)rotate(-90)")
			.style("text-anchor", "middle")
			.text("Čistý příjem domácnosti (Kč)");

	svg.append("path")
			.attr("class", "line")
			.attr("d", chartline(lineData))
			.style("stroke", "#fc9272")
			.style("stroke-width", 6)
			.style("stroke-opacity", 1)
			.style("fill", "none")
			.on("mouseover", function(d) {
				d3.select(this)
				.transition()
				.duration(200)
				.style("opacity", .5)
				var coordinates = [0, 0];
				coordinates = d3.mouse(this);
				var x = coordinates[0];
				var y = coordinates[1];
				svg.append("line")
					.style("stroke", "#fc9272")
					.style("stroke-width", 0.5)
					.attr("x1", xScale(0))
					.attr("y1", coordinates[1])
					.attr("x2", coordinates[0])
					.attr("y2", coordinates[1])
					.style("opacity", 1)
				svg.append("line")
					.style("stroke", "#fc9272")
					.style("stroke-width", 0.5)
					.attr("x1", coordinates[0])
					.attr("y1", yScale(0))
					.attr("x2", coordinates[0])
					.attr("y2", coordinates[1])
					.style("opacity", 1)
				svg.append("text")
					.attr("class", "mouseText")
					.style("stroke", "#fc9272")
					.attr("x", xScale(0)+10)
					.attr("y", coordinates[1]-10)
					.style("font-weight", "normal")
					.style("font-size", "12px")
					.text(Math.round(negativeYScale(coordinates[1]),0))
				svg.append("text")
					.attr("class", "mouseText")
					.style("stroke", "#fc9272")
					.attr("x", coordinates[0]+10)
					.attr("y", yScale(0)-10)
					.style("font-weight", "normal")
					.style("font-size", "12px")
					.text(Math.round(negativeXScale(coordinates[0]),0))
			})
			.on("mousemove", function(d) {
				d3.select(this)
				.transition()
				.duration(200)
				.style("opacity", 1)
				svg.selectAll("line").remove();
				svg.selectAll("text.mouseText").remove();
				d3.select(this)
				.transition()
				.duration(200)
				.style("opacity", .5)
				var coordinates = [0, 0];
				coordinates = d3.mouse(this);
				var x = coordinates[0];
				var y = coordinates[1];
				svg.append("line")
					.style("stroke", "#fc9272")
					.style("stroke-width", 0.5)
					.attr("x1", xScale(0))
					.attr("y1", coordinates[1])
					.attr("x2", coordinates[0])
					.attr("y2", coordinates[1])
					.style("opacity", 1)
				svg.append("line")
					.style("stroke", "#fc9272")
					.style("stroke-width", 0.5)
					.attr("x1", coordinates[0])
					.attr("y1", yScale(0))
					.attr("x2", coordinates[0])
					.attr("y2", coordinates[1])
					.style("opacity", 1)
				svg.append("text")
					.attr("class", "mouseText")
					.style("stroke", "#fc9272")
					.attr("x", xScale(0)+10)
					.attr("y", coordinates[1]-10)
					.style("font-weight", "normal")
					.style("font-size", "12px")
					.text(Math.round(negativeYScale(coordinates[1]),0))
				svg.append("text")
					.attr("class", "mouseText")
					.style("stroke", "#fc9272")
					.attr("x", coordinates[0]+10)
					.attr("y", yScale(0)-10)
					.style("font-weight", "normal")
					.style("font-size", "12px")
					.text(Math.round(negativeXScale(coordinates[0]),0))
			})
			.on("mouseout", function(d) {
				d3.select(this)
				.transition()
				.duration(200)
				.style("opacity", 1)
				svg.selectAll("line").remove();
				svg.selectAll("text.mouseText").remove();
			})

	svg.append("path")
			.attr("class", "line")
			.attr("d", chartline(lineDataBezDavek))
			.style("stroke", "#feb24c")
			.style("stroke-width", 6)
			.style("stroke-opacity", 1)
			.style("fill", "none")
			.on("mouseover", function(d) {
				d3.select(this)
				.transition()
				.duration(200)
				.style("opacity", .5)
				var coordinates = [0, 0];
				coordinates = d3.mouse(this);
				var x = coordinates[0];
				var y = coordinates[1];
				svg.append("line")
					.style("stroke", "#feb24c")
					.style("stroke-width", 0.5)
					.attr("x1", xScale(0))
					.attr("y1", coordinates[1])
					.attr("x2", coordinates[0])
					.attr("y2", coordinates[1])
					.style("opacity", 1)
				svg.append("line")
					.style("stroke", "#feb24c")
					.style("stroke-width", 0.5)
					.attr("x1", coordinates[0])
					.attr("y1", yScale(0))
					.attr("x2", coordinates[0])
					.attr("y2", coordinates[1])
					.style("opacity", 1)
				svg.append("text")
					.attr("class", "mouseText")
					.style("stroke", "#feb24c")
					.attr("x", xScale(0)+10)
					.attr("y", coordinates[1]-10)
					.style("font-weight", "normal")
					.style("font-size", "12px")
					.text(Math.round(negativeYScale(coordinates[1]),0))
				svg.append("text")
					.attr("class", "mouseText")
					.style("stroke", "#feb24c")
					.attr("x", coordinates[0]+10)
					.attr("y", yScale(0)-10)
					.style("font-weight", "normal")
					.style("font-size", "12px")
					.text(Math.round(negativeXScale(coordinates[0]),0))
			})
			.on("mousemove", function(d) {
				d3.select(this)
				.transition()
				.duration(200)
				.style("opacity", 1)
				svg.selectAll("line").remove();
				svg.selectAll("text.mouseText").remove();
				d3.select(this)
				.transition()
				.duration(200)
				.style("opacity", .5)
				var coordinates = [0, 0];
				coordinates = d3.mouse(this);
				var x = coordinates[0];
				var y = coordinates[1];
				svg.append("line")
					.style("stroke", "#feb24c")
					.style("stroke-width", 0.5)
					.attr("x1", xScale(0))
					.attr("y1", coordinates[1])
					.attr("x2", coordinates[0])
					.attr("y2", coordinates[1])
					.style("opacity", 1)
				svg.append("line")
					.style("stroke", "#feb24c")
					.style("stroke-width", 0.5)
					.attr("x1", coordinates[0])
					.attr("y1", yScale(0))
					.attr("x2", coordinates[0])
					.attr("y2", coordinates[1])
					.style("opacity", 1)
				svg.append("text")
					.attr("class", "mouseText")
					.style("stroke", "#feb24c")
					.attr("x", xScale(0)+10)
					.attr("y", coordinates[1]-10)
					.style("font-weight", "normal")
					.style("font-size", "12px")
					.text(Math.round(negativeYScale(coordinates[1]),0))
				svg.append("text")
					.attr("class", "mouseText")
					.style("stroke", "#feb24c")
					.attr("x", coordinates[0]+10)
					.attr("y", yScale(0)-10)
					.style("font-weight", "normal")
					.style("font-size", "12px")
					.text(Math.round(negativeXScale(coordinates[0]),0))
			})
			.on("mouseout", function(d) {
				d3.select(this)
				.transition()
				.duration(200)
				.style("opacity", 1)
				svg.selectAll("line").remove();
				svg.selectAll("text.mouseText").remove();
			})

	svg.append("circle")
			.style("fill", "#2b8cbe")
			.attr("cx", xScale(0))
			.attr("cy", yScale(lineDataBezPrace['y']))
			.attr("r", 7)
			.on("mouseover", function(d) {
				d3.select(this)
					.transition()
					.duration(200)
					.style("opacity", .4)
				svg.append("text")
					.attr("class", "mouseText")
					.style("stroke", "#2b8cbe")
					.attr("x", xScale(0)+10)
					.attr("y", yScale(lineDataBezPrace['y'])-10)
					.style("font-weight", "normal")
					.style("font-size", "12px")
					.text(lineDataBezPrace['y'])
			})
			.on("mouseout", function(d) {
				d3.select(this)
					.transition()
					.duration(200)
					.style("opacity", 1)
				svg.selectAll("line").remove();
				svg.selectAll("text.mouseText").remove();
			})
			.transition()

	svg.append("circle")
			.style("fill", "#de2d26")
			.attr("cx", xScale(hrubyPrijemProGraf))
			.attr("cy", yScale(disponibilniPrijemPoOdecteniNakladuNaBydleniGraf))
			.attr("r", 7)
			.on("mouseover", function(d) {
				d3.select(this)
					.transition()
					.duration(200)
					.style("opacity", .4)
				svg.append("line")
					.style("stroke", "#de2d26")
					.style("stroke-width", 0.5)
					.attr("x1", xScale(0))
					.attr("y1", yScale(disponibilniPrijemPoOdecteniNakladuNaBydleniGraf))
					.attr("x2", xScale(hrubyPrijemProGraf))
					.attr("y2", yScale(disponibilniPrijemPoOdecteniNakladuNaBydleniGraf))
					.style("opacity", 1)
				svg.append("line")
					.style("stroke", "#de2d26")
					.style("stroke-width", 0.5)
					.attr("x1", xScale(hrubyPrijemProGraf))
					.attr("y1", yScale(0))
					.attr("x2", xScale(hrubyPrijemProGraf))
					.attr("y2", yScale(disponibilniPrijemPoOdecteniNakladuNaBydleniGraf))
					.style("opacity", 1)
				svg.append("text")
					.attr("class", "mouseText")
					.style("stroke", "#de2d26")
					.attr("x", xScale(0)+10)
					.attr("y", yScale(disponibilniPrijemPoOdecteniNakladuNaBydleniGraf)-10)
					.style("font-weight", "normal")
					.style("font-size", "12px")
					.text(disponibilniPrijemPoOdecteniNakladuNaBydleniGraf)
				svg.append("text")
					.attr("class", "mouseText")
					.style("stroke", "#de2d26")
					.attr("x", xScale(hrubyPrijemProGraf)+10)
					.attr("y", yScale(0)-10)
					.style("font-weight", "normal")
					.style("font-size", "12px")
					.text(hrubyPrijemProGraf)
			})
			.on("mouseout", function(d) {
				d3.select(this)
					.transition()
					.duration(200)
					.style("opacity", 1)
				svg.selectAll("line").remove();
				svg.selectAll("text.mouseText").remove();
			})
			.transition()

	svg.append("circle")
			.style("fill", "#de2d26")
			.attr("cx", 21)
			.attr("cy", 15)
			.attr("r", 5)

	svg.append("text")
			.attr("x", 50)
			.attr("y", 20)
			.text("kolik rodině zbude po zaplacení nájmu nyní")
			.attr("fill", "#de2d26")

	svg.append("path")
			.attr("class", "line")
			.attr("d", chartline([{x: -0.23*xMax, y:1.58*yMax}, {x: -0.21*xMax, y: yMax*1.61}, {x: -0.19*xMax, y: yMax*1.55}, {x: -0.17*xMax, y: yMax*1.58}]))
			.style("stroke", "#fc9272")
			.style("stroke-width", 3)
			.style("stroke-opacity", 1)
			.style("fill", "none")

	svg.append("text")
			.attr("x", 50)
			.attr("y", 40)
			.text("kolik by rodině zbylo, kdyby první rodič vydělával víc/míň")
			.attr("fill", "#fc9272")

	svg.append("circle")
			.style("fill", "#2b8cbe")
			.attr("cx", 21)
			.attr("cy", 55)
			.attr("r", 5)

	svg.append("text")
			.attr("x", 50)
			.attr("y", 60)
			.text("kolik by rodině zbylo, kdyby byl první rodič bez práce")
			.attr("fill", "#2b8cbe")

	svg.append("path")
			.attr("class", "line")
			.attr("d", chartline([{x: -0.23*xMax, y:1.34*yMax}, {x: -0.21*xMax, y: yMax*1.37}, {x: -0.19*xMax, y: yMax*1.31}, {x: -0.17*xMax, y: yMax*1.34}]))
			.style("stroke", "#feb24c")
			.style("stroke-width", 3)
			.style("stroke-opacity", 1)
			.style("fill", "none")

	svg.append("text")
			.attr("x", 50)
			.attr("y", 80)
			.text("kolik by rodině zbylo, kdyby nepobírala sociální dávky")
			.attr("fill", "#feb24c")

}

d3.select("#fallback").remove();

//new Tooltip().watchElements();

$("form").on('submit', function(ev) {
	var isOk = valuesChanged();
	if(isOk) {
		uka();
	}
	ev.preventDefault();
});

function checkForMax($div) {
	var $lis = $div.find(".items li");
	var max = $div.data("max-items");
	var $button = $div.find(".jsAddItem");
	if(max && (max <= $lis.length)) {
		$button.hide();
	}
	else {
		$button.show();
	}
}

$("body").on("click", ".jsAddItem", function(ev) {
	var $div = $(this).closest("div");
	addItem($div);
	ev.preventDefault();

});

$("body").on("click", ".jsRemoveItem", function(ev) {
	$div = $(this).closest("div[data-name]");
	$(this).closest("li").remove();
	checkForMax($div);
	ev.preventDefault();

});

$('[data-toggle="tooltip"]').tooltip();

// úvodní nastavení, modelová rodinka
addItem($("#rodiceForm"), {value: 11000});
addItem($("#detiForm"), {value: 5});
addItem($("#detiForm"), {value: 8});
$("[name=najemInput]").val(4000);
$("[name=energieInput]").val(2000);
$("form").submit();