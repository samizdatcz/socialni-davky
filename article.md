---
title: "Kalkulačka sociálních dávek: vyzkoušejte si, jak uživit rodinu z minimální mzdy"
perex: "Zvolte si počet dospělých, dětí a důchodců, jejich příjmy a výdaje za bydlení. Kalkulačka vám ukáže, na jaké dávky má rodina nárok a jak vysoký plat by musel jeden z rodičů dostat, aby bylo výhodné nechat se zaměstnat."
description: "Zvolte si počet dospělých, dětí a důchodců, jejich příjmy a výdaje za bydlení. Kalkulačka vám ukáže, na jaké dávky má rodina nárok a jak vysoký plat by musel jeden z rodičů dostat, aby bylo výhodné nechat se zaměstnat."
authors: ["Jan Boček, Jan Cibulka, Petr Kočí"]
published: "27. března 2017"
coverimg: https://interaktivni.rozhlas.cz/data/socialni-davky/www/media/coverimg.jpg
coverimg_note: "Repro <a href='https://en.wikipedia.org/wiki/The_Potato_Eaters'>Vincent van Gogh: De Aardappeleters</a>"
# socialimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
url: "socialni-davky"
libraries: [jquery, d3, "bootstrap/js/bootstrap.min.js"]
styles: ["bootstrap/css/bootstrap.min.css", "bootstrap/css/bootstrap-theme.min.css"]
recommended:
  - link: https://interaktivni.rozhlas.cz/zakladni-prijem/
    title: Základní příjem: Otázky a odpovědi
    perex: Švýcaři o víkendu v referendu rozhodují o zavedení základního nepodmíněného příjmu. S možností, že by vláda místo komplikovaných sociálních dávek mohla platit každému občanovi fixní částku, koketuje i Kanada (na snímku premiér Justin Trudeau) nebo Finsko. Jaké jsou výsledky prvních experimentů?
    image: https://interaktivni.rozhlas.cz/zakladni-prijem/media/0cc52f6cd963f8b4b63663198445ac88/1920x_.jpeg
  - link: https://interaktivni.rozhlas.cz/rozdelena-spolecnost/
    title: Rozdělená společnost? Čechy proti sobě staví věk a vzdělání
    perex: Ovládnou i Česko nespokojení voliči, kteří věří na jednoduchá řešení?
    image: https://interaktivni.rozhlas.cz/rozdelena-spolecnost/media/7d29d198e53cdad18df2d865db3c0f5d/450x_.jpg
  - link: https://interaktivni.rozhlas.cz/hazard/
    title: Hazardní byznys roste. Automaty mizí, nahrazují je nebezpečné online sázky
    perex: Čeští hazardní hráči loni vsadili rekordních 152 miliard korun.
    image: https://interaktivni.rozhlas.cz/hazard/media/2521b6ae119fecada5488c82f544af65/450x_.jpeg
  - link: https://interaktivni.rozhlas.cz/sudety/
    title: Existují Sudety? Hranice jsou zřetelné i sedmdesát let po vysídlení Němců
    perex: Statistiky nezaměstnanosti, kvality vzdělání nebo volební účasti v Česku dodnes kopírují hranice historických Sudet.
    image: https://interaktivni.rozhlas.cz/sudety/media/e9bae3cb82a75d9f64058b3dc984789a/600x_.jpg
---

„Pracovat se musí. Já bych ty sociálky úplně zrušila a ať jdou všichni pracovat. To by měli zavést znova. Za příživnictví by měli zavírat jako dřív,“ říká žena se čtyřmi dětmi a před rozvodem, která pracuje za minimální mzdu. Aby vyžila, pobírá dávky v hmotné nouzi.

Anonymní žena je jedním z respondentů studie [Mezi dávkami a prací, která není](https://www.vlada.cz/assets/clenove-vlady/pri-uradu-vlady/jiri-dienstbier/aktualne/Vyzkum---Mezi_davkami_a_praci.pdf), která vznikla pro Úřad vlády. Studie zkoumala, jak nízkopříjmové rodiny vycházejí s penězi a jak využívají sociální dávky.

„Naše analýza ukázala hlavně to, že nízká minimální mzda spolu se systémem sociálních dávek příliš nemotivuje k práci,“ tvrdí Lucie Trlifajová z Centra pro Společenské otázky – SPOT, která studii vedla. „Vzhledem k tomu, jak nízké jsou mzdy lidí s horším vzděláním, kteří mezi příjemci dávek převládají, zůstává mnoho rodin pod hladinou chudoby i s prací.“

Stejný pohled má i další respondentka studie, tentokrát zaměstnankyně Úřadu práce. „Když má člověk práci, je jen lehce nad tím, než když je v hmotné nouzi. Ve chvíli, kdy žadatelé vystoupí z hmotné nouze díky práci, tak se jim zvýší náklady na řadu věcí: léky kvůli poplatkům v lékárně, družiny, domácí práce... Pokud tu práci nechtějí dělat kvůli smyslu, nemá to finančně žádný význam,“ dodává úřednice.

Následující kalkulačka ukazuje, jak by se žilo osamocenému rodiči se dvěma dětmi a práci za minimální mzdu. Tmavě červená tečka v grafu vpravo dole říká, jaké peníze jim – po zaplacení nájmu – zůstanou na jídlo, oblečení a ostatní náklady. Světle červená linka ilustruje, kolik by jim zbylo, kdyby se se změnil příjem hlavy rodiny. Modrá tečka ukazuje, o kolik by si pohoršili, kdyby byl první rodič bez práce. Žlutá linka ukazuje situaci rodiny bez dávek.

Změnou parametrů si můžete poskládat vlastní modelovou rodinu.

<p id=fallback>Váš prohlížeč je bohužel zastaralý a nepodporuje interaktivní vizualizace. Doporučujeme jej aktualizovat nebo <a target=_blank href="http://www.browserchoice.eu/">stáhnout jiný</a></p>

<div class="simulator">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <form class="form-inline">
            <h3>Složení domácnosti</h3>

            <fieldset>
              <div id="rodiceForm" data-name="rodiceInput" data-max-items="2" data-step="100" data-default-value="11000" data-label="Hrubý příjem">
                <h4>Rodiče</h4>
                <ol class="items">
                </ol>
                <a href="" class="jsAddItem btn btn-default">+ Přidat dalšího</a>
              </div>
              <hr>
              <div  id="dospeliForm" data-name="dospeliInput" data-max-items="2" data-step="100" data-default-value="11000" data-label="Hrubý příjem">
                <strong data-toggle="tooltip" title="Ve společné domácnosti mohou kromě rodiny žít další dospělí nebo pracující děti nad 15 let. Jejich příjem se počítá do příjmu domácnosti.">Další dospělí <span class="glyphicon glyphicon-question-sign"></span></strong>
                <ol class="items">
                </ol>
                <a href="" class="jsAddItem btn btn-default">+ Přidat dalšího</a>
              </div>
            </fieldset>

            <fieldset>
              <h4>Děti do 15 let</h4>
              <div id="detiForm" data-name="detiInput" data-max-items="5" data-step="0.5" data-max="15" data-min="0" data-default-value="8" data-label="Věk">
                <ol class="items">
                </ol>
                <a href="" class="jsAddItem btn btn-default">+ Přidat další</a>
              </div>
              <hr>
              <div id = "studentiForm" class="form-group">
                <label>Studenti 15 až 26 let</label>
                <select class="form-control" name="studentiInput">
                  <option value="0">0</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                </select>
              </div>
            </fieldset>

            <fieldset>
              <div id="duchodciForm" data-name="duchodciInput" data-max-items="2" data-step="100" data-default-value="9000" data-label="Důchod">
                <h4>Důchodci</h4>
                <ol class="items">
                </ol>
                <a href="" class="jsAddItem btn btn-default">+ Přidat dalšího</a>
              </div>
            </fieldset>

            <fieldset>
            <h3>Bydlení</h3>
              <div id = "najemForm">
                <div class="form-inline">
                  <div class="form-group">
                    <select id="typBydleni" data-toggle="tooltip" title="U bytu v osobním nebo družstevním vlastnictví, případně při pobytu na ubytovně, zadejte do vedlešího políčka 'Nájem' měsíční náklady." class="form-control" name="typBydleni">
                      <option value="bytNajem">V nájmu</option>
                      <option value="bytVlastni">Vlastní byt</option>
                      <option value="ubytovna">Ubytovna</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="najemInput" style="padding-left: 10px;padding-right: 5px;">Nájem:</label>
                    <input class="form-control" type="text" id="najemInput" name="najemInput" step="100" value="6000">
                  </div>
                </div>
              </div>
              <hr>
              <div id="energieForm">
                <label for="energieInput" style="padding-right: 5px;">Energie a další náklady na bydlení:</label>
                <input type="text" class="form-control" name="energieInput" step="100" value = "4000" />
              </div>
              <hr>
              <div id = "obecForm" class="form-group">
                <label for="obecInput" style="padding-right: 5px;">Velikost obce:</label>
                <select id="obecInput" class="form-control" name="obecInput">
                  <option value="0">do 10 tisíc</option>
                  <option value="1">10 až 50 tisíc</option>
                  <option value="2" selected="selected">50 až 100 tisíc</option>
                  <option value="3">nad 100 tisíc (mimo Prahu)</option>
                  <option value="4">Praha</option>
                </select>
              </div>
              <hr>
              <div id = "trvalyForm">
                <label for="trvalyInput" data-toggle="tooltip" title='Pokud má některá z osob posuzovaných ve společné domácnosti trvalé bydliště jinde, pro některé poplatky se do domácnosti nepočítá.' style="padding-right: 5px;">Členové domácnosti s trvalým pobytem jinde <span class="glyphicon glyphicon-question-sign">:</label>
                <select class="form-control" name="trvalyInput">
                  <option value="0">0</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                </select>
              </div><br>
            </fieldset>

            <button type="submit" class="btn btn-primary" name="submit">Všechno spočítat!</button>
            <br><br>

          </form>
        </div>
        <div class="col-md-7">
          <div id="results">
          </div>
          <h3>Co by domácnosti přinesl vyšší příjem ze zaměstnání?</h3>
          <div id="chart">
          </div>
        </div>
      </div>
    </div>
  </div>

## Co v kalkulačce nenajdete

Kalkulačka nemůže obsáhnout všechny dávky ani všechny okolnosti, které vstupují do výpočtu. Také proto, že některé jsou plně v rukou úředníků. Například u příspěvku na živobytí nebo doplatku na bydlení rozhodují o tom, zda je rodina v hmotné nouzi a nemá žádný majetek, jehož prodejem by si mohla polepšit. Například automobil je v některých případech důvod pro neudělení podpory, jinde nevadí.

Výpočet tedy představuje modelovou domácnost: jednoho až dva rodiče, kteří jsou buď zaměstnaní, nebo bez práce. Pro jednoduchost nepočítáme s jinými pracovními úvazky – pro rodiny s nízkou kvalifikací jsou typické krátkodobé brigády na dohodu o provedení práce. Práci načerno nezahrnujeme vůbec.

<div data-bso="1"></div>

Kromě rodičů mohou k příjmu společné domácnosti přispívat také další dospělí žijící ve stejném bytě a důchodci. Naopak u studentů pro jednoduchost počítáme s tím, že se věnují pouze škole a nepracují. Nikdo v modelové rodině nemá zdravotní postižení.

Část příspěvků – takzvané pojistné dávky, tedy podpora v nezaměstnanosti, nemocenská a mateřská – jsou vázané na předchozí platby pojištění a rodina si je tedy předplácí. Nízkopříjmové rodiny na ně mají nárok spíš výjimečně, a ani v kalkulačce je proto nenajdete.

Pro rodiny s nízkými příjmy jsou typické jiné příspěvky. Jednak jsou to dávky státní sociální podpory: přídavek na dítě, příspěvek na bydlení a také rodičovský příspěvek, na který ovšem mají nárok všechny domácnosti s dítětem. Ještě podstatnější jsou pak dávky v hmotné nouzi, konkrétně příspěvek na živobytí, doplatek na bydlení a mimořádná okamžitá pomoc. Na ty mají nárok pouze rodiny s příjmy pod hranicí chudoby, navíc bez úspor a majetku, který by mohly prodat.

Ve výpočtu najdete všechny zmíněné dávky s výjimkou té poslední. Dávka mimořádné okamžité pomoci je posuzována na jednotlivých úřadech individuálně – jejich vyplácení se tedy může mezi různými úřady lišit. V některých regionech například může sloužit vedle drobných výdajů i k zaplacení zálohy na nájem, opravě pračky nebo řešení dalších nečekaných výdajů. Jinde se tomuto využití brání.

Vedle vyjmenovaných dávek je pro nízkopříjmové rodiny relevantní ještě jednorázové porodné ve výši 13 tisíc korun na první a 10 tisíc korun na druhé dítě. Jelikož nejde o pravidelnou dávku, ani porodné v kalkulačce nenajdete.

U některých dávek si po zobrazení výsledku můžete přečíst další okolnosti, podstatné pro jejich nárok a vyplácení. Zdrojem pro výpočty jsou ve všech případech stránky ministerstva práce a sociálních věcí.

## Práce může znamenat paradoxní snížení příjmu

Kalkulačka ukazuje, že pro většinu nízkopříjmových rodin je motivace hledat si zaměstnání velmi nízká. Pro modelovou domácnost jednoho rodiče s dvěma dětmi, žijící v nájemním bytě a čerpající všechny příspěvky, na které mají nárok, je rozdíl mezi nezaměstnaností a minimální mzdou asi 1500 korun. Pokud by nástup do práce znamenal další náklady – typicky na cestování nebo stravu – může být práce ekonomicky nevýhodná. S vyšší mzdou navíc přicházejí nízkopříjmové rodiny o nárok na některé dávky, takže celková výše čistých příjmů zůstává pro většinu rodin téměř neměnná. Finanční situace se u zmíněné rodiny zlepší až při hrubém příjmu domácnosti kolem 20 tisíc korun.

U některých rodin může dokonce nastat paradoxní situace, kdy vyšší hrubý příjem znamená zhoršení ekonomické situace. Odborně se takový moment nazývá *nízkopříjmová past*. Nastává v okamžiku, kdy se rodině zvedne příjem ze zaměstnání o tolik, že ztratí nárok na některou dávku, přitom samotné zvýšení příjmu je nižší než dávka. Hlavní příčinou nízkopříjmových pastí jsou v Česku slevy na daních na děti. Při třech a více dětech si domácnost může přilepšit – pokud je někdo z rodiny zaměstnaný – až o pět tisíc korun.

<aside class="small">
  <figure>
    <img src="https://interaktivni.rozhlas.cz/socialni-davky/media/past.png" width="300px">
  </figure>
  <figcaption>
    Světle červená linka ukazuje nízkopříjmovou past: pro samotného rodiče žijícího s třemi dětmi by vyšší hrubý příjem znamenal propad skutečného příjmu.
  </figcaption>
</aside>

Nízká minimální mzda také znamená, že řada rodin zůstává v hmotné nouzi, i když pracují. Příjem na hlavu se v takových rodinách pohybuje kolem čtyř tisíc korun, často klesne i níž.

„Tyto velmi nízké příjmy nejsou pro rodiny dočasným výpadkem, jde obvykle o dlouhodobý stav,“ vypráví Trlifajová.  „Sociální dávky pomůžou s pokrytím základních nákladů, jako je bydlení a jídlo, moc dalšího z nich ovšem nezaplatíte. Domácnosti často kombinují dávky s pomocí širší rodiny, ale také s půjčkami a prací načerno, aby nějak vyšly. Velká část domácností, která pobírá dlouhodobě dávky, je nejen zadlužená, ale často má i několik exekucí. Alespoň občasná práce na černo se stává nezbytnou pro pokrytí základních nákladů. Špatně zaplacená, navíc často dočasná práce z tohoto koloběhu vystoupit nepomůže.“

## O podporu si neříká každý, chybí informace

Kalkulačka počítá s tím, že domácnost čerpá veškeré dávky, na které má nárok. Tak tomu ale často není.

„Pakárna. Chtějí papíry, které si mohou vyžádat elektronicky od jiného úřadu. Když jsem se ptala, proč to chtějí, tak prý proto, aby nezaměstnaní aspoň něco dělali. Když pracujete, je byrokracie ještě víc, musíte chodit dokládat měsíční příjmy,“ popisuje jednání na Úřadu práce další z respondentek studie, rozvedená žena se dvěma dětmi.

„Na pracáku nám říkali, že když jsme v hmotné nouzi, tak nemáme mít internet,“ doplňuje ji další.

„Část domácností sociální dávky nepobírá, i když má nárok,“ vysvětluje Lucie Trlifajová z Centra pro Společenské otázky – SPOT. „Za prvé, celý proces je stigmatizující. U hmotné nouze musí člověk jednak doložit, že kromě pár výjimek nemá žádný majetek, ale také musí každý měsíc dokládat svůj příjem a být připraven na nečekané kontroly. Žádat o dávky je vnímáno jako ponižující, řada lidí v rozhovorech popisovala, jak se cítí méněcenní, jako spodina.“

„Řada rodin také nemá představu o tom, na jaké dávky má nárok. Více vědí ti, v jejichž okolí má někdo se systémem zkušenost. Komplikovaný systém sociálních dávek a nedostatek srozumitelných informací znamenají, že většina je odkázaná na ochotu úředníků. Několikrát jsem se setkala se situací, kdy úředníci žadatelům nepravdivě tvrdili, že na některou dávku nemají nárok. Sami úředníci jsou ale přetížení, špatně placení a často znají jen tu část systému, kterou administrují.“

„Výsledkem je, že zejména nízkopříjmové rodiny, které bez dávek nejspíš jakžtakž přežívají, často úředník odradí. Žijí pak podobně jako domácnosti, které berou dávky v plné výši. Navíc na rozdíl od rodin v hmotné nouzi nemají nárok na pomoc při nečekaných výdajích, jako třeba rozbitá pračka. To celkem pochopitelně přispívá k pocitu frustrace a nespravedlnosti,“ doplňuje Trlifajová.

## Odměna za veřejnou službu: nesnížení dávek

Od letošního února se navíc v Česku již potřetí zavádí takzvaná veřejná služba, tedy práce za dávky. Předchozí variantu veřejné služby, zavedenou ministrem Drábkem, napadl v roce 2012 Ústavní soud. Šlo o ústavou nepovolené nucené práce: pod pohrůžkou vyřazení z dávkového systému na šest měsíců byly veřejné práce zcela zdarma a navíc povinné.

Novela z roku 2013, která vznikla v úřednické vládě Jiřího Rusnoka, řeší odměnu šalamounsky: kdo veřejně prospěšné práce odmítne, tomu se dávky v hmotné nouzi sníží; kdo odpracuje dvacet hodin měsíčně, tomu zůstanou v současné podobě. „Odměnou“ je tedy zachování současného stavu.